package org.cmu.placemark.console.main

import mu.KotlinLogging
import org.cmu.placemark.console.models.PlacemarkModel

private val logger = KotlinLogging.logger{}

var title: String = ""
var description: String = ""
var placemarks = ArrayList<PlacemarkModel>()


fun main(args: Array<String>){
    logger.info{"Launching Placemark Console App"}
    println("Placemark Kotlin App")

    var input : Int
    dummyData()
    do{
        input = menu()
        when(input){
            1 -> addPlaceMark()
            2 -> updatePlacemark()
            3 -> listPlacemarks()
            4 -> searchPlacemark()
            -1 ->println("Exiting App")
            else -> println("Invalid Option")
        }

        println()
    }
        while(input != -1)
        logger.info{"Shutting Down PlaceMark Console App"}
}

fun menu() : Int {
    var option : Int
    var input: String? = null


    println("Main Menu")
    println(" 1. Add Placemark")
    println(" 2. Update Placemark")
    println(" 3. List All Placemarks")
    println(" 4. Search Placemarks")
    println("-1. Exit")
    println()
    print("Enter an integer : ")
    input = readLine()!!
    option = if (input.toIntOrNull() != null && !input.isEmpty())
        input.toInt()
    else
        -9
    return option
}

fun addPlaceMark(){
    var placemark = PlacemarkModel()

    println("Add Placemark")
    println()
    print("Enter a Title: ")
    placemark.title = readLine()!!
    print("Enter a Description")
    placemark.description = readLine()!!

    if(placemark.title.isNotEmpty() && placemark.description.isNotEmpty()){
        placemarks.add(placemark.copy())
        placemark.id++
        logger.info("Placemark Added: [$placemark]")
    }
    else
        logger.info("Placemark not added")
}


fun updatePlacemark() {
    println("Update Placemark")
    println()
    listPlacemarks()
    var searchId = getId()
    val aPlacemark = search(searchId)

    if(aPlacemark != null) {
        print("Enter a new Title for [ " + aPlacemark.title + " ] : ")
        aPlacemark.title = readLine()!!
        print("Enter a new Description for [ " + aPlacemark.description + " ] : ")
        aPlacemark.description = readLine()!!
        println(
            "You updated [ " + aPlacemark.title + " ] for title " +
                    "and [ " + aPlacemark.description + " ] for description"
        )
    }
    else
        println("Placemark Not Updated...")
}

fun listPlacemarks() {
    println("List All Placemarks")
    println()
    placemarks.forEach { logger.info("${it}")}
    println()
}

fun getId() : Long{
    var strId : String? //holds user input
    var searchId : Long //holds converted id
    print("Enter id to Search/Update")
    strId = readLine()!!
    searchId = if(strId.toLongOrNull() != null && !strId.isEmpty())
        strId.toLong()
    else
        -9
    return searchId
}

fun search(id: Long) : PlacemarkModel? {
    var foundPlacemark : PlacemarkModel? = placemarks.find { p -> p.id == id}
    return foundPlacemark
}

fun searchPlacemark(){
    var searchId = getId()
    val aPlacemark = search(searchId)

    if(aPlacemark != null)
        println("Placemark details [$aPlacemark]")

    else
        println("Placemark Not Found...")

}

fun dummyData(){
    placemarks.add(PlacemarkModel(1, "New York New York", "So Good They Named It Twice"))
    placemarks.add(PlacemarkModel(2, "Ring of Kerry", "Some place in the Kingdom"))
    placemarks.add(PlacemarkModel(3, "Waterford City", "You get great Blaas Here!!"))
}